package codingtest;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;

import codingtest.domain.Card;
import codingtest.domain.Deck;



/**
 * 
 * @Author Chaturika.
 *
 */
public class GameExecutor {

	private static final int PLAYERS_LOWER_LIMIT = 1;
	private static final int PLAYERS_UPPER_LIMIT = 6;

	/**
	 * 
	 * @param initialCardDeck
	 *            - Shuffled card set with 52 cards
	 * @param numberOfPlayers
	 * @throws InterruptedException
	 */
	public void startGame(List<Card> initialCardDeck, int numberOfPlayers)
			throws InterruptedException {
		CardGame ruleMonitor = CardGame.getRuleMonitor();
		ruleMonitor.setCardDeck(new LinkedList<Card>(initialCardDeck));
		ruleMonitor.setNumberOfPlayers(numberOfPlayers);

		ruleMonitor.allocate2InitialCardsPerPlayer();
		int i = 1;
		do {
			System.out.println(">>>>>>>> Iteration  " + i
					+ " >>>>>>>>>>>> card deck size "
					+ ruleMonitor.getCardDeckSize());
			ruleMonitor.printPlayersPosessions();
			ruleMonitor.updatePlayersPosition();

			i++;

		} while (!ruleMonitor.isGameFinished());
		Stack<Integer> winningOrder = ruleMonitor.getWinningOrder();
		System.out.println("Game over winner order is   " + winningOrder.pop());

	}

	/**
	 * Populate the deck, shuffles it
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	public List<Card> getInitialDeck() throws InterruptedException {
		List<Card> deck = Deck.getInstance().getDeck();
		shuffleDeck(deck);
		return deck;
	}

	private void shuffleDeck(List<Card> deck) {
		long seed = System.nanoTime();
		Collections.shuffle(deck, new Random(seed));

	}

	public static void main(String[] args) throws InterruptedException {

		System.out.println("How many players do you want to include >>  : ");
		Scanner in = new Scanner(System.in);
		int numberOfPlayers = in.nextInt();
		if (numberOfPlayers > PLAYERS_LOWER_LIMIT
				&& numberOfPlayers <= PLAYERS_UPPER_LIMIT) {

			GameExecutor gameExecutor = new GameExecutor();
			List<Card> initialDeck = gameExecutor.getInitialDeck();
			gameExecutor.startGame(initialDeck, numberOfPlayers);
		}
		else{
			System.out.println("Number of players should be at least 2 and at most 6 ");
		}

	}

}
