package codingtest.domain;

/**
 * 
 * @Author Chaturika. 
 * This class represents the card, the category and value it holds
 * Category - Heart Spade Diamond club Value - 2-10 ACE KING JACK QUEEN
 *
 */
public class Card {
	private String category;
	private String value;

	public Card(String category, String value) {
		super();
		this.category = category;
		this.value = value;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
