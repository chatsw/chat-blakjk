package codingtest.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * This class creates the initial DEck with all cards required
 * @Author Chaturika. 
 *
 */
public class Deck {
	private static Deck deck = null;
	
	public static Deck getInstance(){
		if (deck == null){
			deck = new Deck();
		}
		return deck;
	}

	private final String[] cardCategories = { "DIOMANDS", "CLUBS", "HEARTS",
			"SPADES" };

	private final String[] cardValues = { "ACE", "2", "3", "4", "5", "6", "7",
			"8", "9", "10", "JACK", "QUEEN", "KING" };

	public List<Card> getDeck() {
		List<Card> deckList = new ArrayList<Card>();
		populateDeck(deckList);
		return deckList;
	}

	/**
	 * Construct a Deck with standard 52 size - all combinations
	 * 
	 * @param deckList
	 */
	private void populateDeck(List<Card> deckList) {

		for (int categoryIndex = 0; categoryIndex < cardCategories.length; categoryIndex++) {
			for (int valuesIndex = 0; valuesIndex < cardValues.length; valuesIndex++) {

				Card card = new Card(cardCategories[categoryIndex],
						cardValues[valuesIndex]);
				deckList.add(card);

			}

		}

	}

}
