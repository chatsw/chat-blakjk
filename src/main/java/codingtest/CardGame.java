package codingtest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import codingtest.domain.Card;



/**
 * This class monitors the progress of the game
 * @Author Chaturika. 
 *
 */
public class CardGame {

	private final static String STATUS_STICK = "STATUS_STICK";
	private final static String STATUS_HIT = "STATUS_HIT";
	private final static String STATUS_GO_BUST = "STATUS_GO_BUST";
	private final static String STATUS_GAME_FINISHED = "STATUS_GAME_FINISHED";
	private static final int HIT_BOUNDARY = 17;
	private static final int STICK_BOUNDARY = 21;
	private int numberOfPlayers = 0;

	// A map to contain player id and their status
	private Map<Integer, String> playerStatus = new HashMap<Integer, String>();

	// A map to maintain player ID and the cards they posses
	private Map<Integer, List<Card>> playerCardMap = new ConcurrentHashMap<Integer, List<Card>>();

	// Card deck
	private Queue<Card> cardDeck = null;
	private boolean gameFinished = false;

	private static CardGame ruleMonitor = null;

	private CardGame() {

	}

	public static CardGame getRuleMonitor() {
		if (ruleMonitor == null) {
			ruleMonitor = new CardGame();
		}
		return ruleMonitor;
	}

	public void setCardDeck(Queue<Card> cardDeck) {
		this.cardDeck = cardDeck;
	}

	public int getCardDeckSize() {
		return this.cardDeck.size();
	}

	// Checks if game is finished
	public boolean isGameFinished() {

		if (isEveryOneStick()) {
			this.gameFinished = true;
			System.out
					.println(">>>>>>>>>>>>>>>>>>>>>>>>> All players stick in the round");
		}
		if (playerCardMap.size() == 1) {
			this.gameFinished = true;
			System.out
					.println(">>>>>>>>>>>>>>>>>>>>>>>>> There is only one player  left in the game because all others have gone bust");
		}
		if (cardDeck.size() < 1) {
			this.gameFinished = true;
			System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>> No cards  left");
		}
		return this.gameFinished;

	}

	/**
	 * Allocate 2 cards per user
	 * 
	 * @param cardDeck
	 * @param numberOfUsers
	 */
	public void allocate2InitialCardsPerPlayer() {

		for (int i = 1; i <= this.numberOfPlayers; i++) {
			List<Card> cardList = new ArrayList<Card>();
			cardList.add(cardDeck.poll());
			cardList.add(cardDeck.poll());
			playerCardMap.put(i, cardList);

		}

	}

	public void setNumberOfPlayers(int numberOfPlayers) {
		this.numberOfPlayers = numberOfPlayers;
	}

	/**
	 * This method iterate through the map that stores user vs cards collection
	 * map and check if any user has won, or if the user should leave the game
	 * 
	 * @throws InterruptedException
	 */
	public void updatePlayersPosition() throws InterruptedException {
		if (playerCardMap.size() == 1) {
			this.gameFinished = true;
		} else {
			Set<Integer> playerIdSet = playerCardMap.keySet();
			Iterator<Integer> playerIdIterator = playerIdSet.iterator();
			while (!gameFinished && playerIdIterator.hasNext()) {
				int playerId = playerIdIterator.next();
				List<Card> cardsPerUser = playerCardMap.get(playerId);
				String status = getStatus(cardsPerUser);
				if (!STATUS_GAME_FINISHED.equals(status)) {

					if (STATUS_GO_BUST.equalsIgnoreCase(status)) {
						// player needs to be removed from game
						playerCardMap.remove(playerId);
						numberOfPlayers--;
						System.out.println("User ID " + playerId
								+ " removed from game");
						Thread.sleep(2000);
					} else {
						if (STATUS_HIT.equalsIgnoreCase(status)) {
							System.out.println("User ID " + playerId
									+ " takes another card");
							// update user's card collection
							cardsPerUser.add(cardDeck.poll());
							playerCardMap.remove(playerId);
							playerCardMap.put(playerId, cardsPerUser);

							Thread.sleep(2000);

						}
						playerStatus.put(playerId, status);
					}

				} else {
					System.out.println("Player hits 21 exactly, player id= " + playerId
							+ " won the game");
				}
			}
		}

	}

	private String getStatus(List<Card> cardsPerUser) {
		String status = null;
		int sumOfCards = getSumOfCards(cardsPerUser);
		if (sumOfCards < HIT_BOUNDARY) {
			status = STATUS_HIT;
		} else if (sumOfCards >= HIT_BOUNDARY && sumOfCards < STICK_BOUNDARY) {
			status = STATUS_STICK;
		} else if (sumOfCards > STICK_BOUNDARY) {
			status = STATUS_GO_BUST;
		} else if (sumOfCards == STICK_BOUNDARY) {
			status = STATUS_GAME_FINISHED;
			this.gameFinished = true;
		}
		return status;

	}

	/**
	 * Compute the sum of numeric value of the collection of cards
	 * @param cardsPerUser
	 * @return
	 */
	private int getSumOfCards(List<Card> cardsPerUser) {

		int cardSum = 0;
		for (Card card : cardsPerUser) {
			int cardNumericValue = -1;
			switch (card.getValue()) {
			case "ACE":
				cardNumericValue = 11;
				break;
			case "2":
				cardNumericValue = 2;
				break;
			case "3":
				cardNumericValue = 3;
				break;
			case "4":
				cardNumericValue = 4;
				break;
			case "5":
				cardNumericValue = 5;
				break;
			case "6":
				cardNumericValue = 6;
				break;
			case "7":
				cardNumericValue = 7;
				break;
			case "8":
				cardNumericValue = 8;
				break;
			case "9":
				cardNumericValue = 9;
				break;
			case "10":
				cardNumericValue = 10;
				break;
			case "KING":
				cardNumericValue = 10;
				break;
			case "QUEEN":
				cardNumericValue = 10;
				break;
			case "JACK":
				cardNumericValue = 10;
				break;

			}
			cardSum = cardSum + cardNumericValue;
		}

		return cardSum;

	}

	/**
	 * Itegarte through the player Status map and check if everyone is in stick status
	 * @return
	 */
	private boolean isEveryOneStick() {
		boolean isEveryoneStick = false;

		int stickPlayerCount = 0;
		Set<Integer> playersSet = playerStatus.keySet();
		Iterator<Integer> playersIterator = playersSet.iterator();
		while (playersIterator.hasNext()) {
			int playerId = playersIterator.next();
			String status = playerStatus.get(playerId);
			if (STATUS_STICK.equals(status)) {
				stickPlayerCount++;
			}

		}
		if (stickPlayerCount == numberOfPlayers) {
			isEveryoneStick = true;
		}
		return isEveryoneStick;

	}

	public Stack<Integer> getWinningOrder() {

		int winningSum = 0;
		Stack<Integer> winnerIdList = new Stack<Integer>();
		Set<Integer> playerSet = playerCardMap.keySet();
		Iterator<Integer> playerIterator = playerSet.iterator();
		while (playerIterator.hasNext()) {
			int playerId = playerIterator.next();
			List<Card> playersCardsList = playerCardMap.get(playerId);

			for (Card card : playersCardsList) {
				int sum = getSumOfCards(playersCardsList);
				if (sum > winningSum) {
					winningSum = sum;
					winnerIdList.push(playerId);
				}

			}
		}
		return winnerIdList;
	}

	public void printPlayersPosessions() {
		Set<Integer> playerSet = playerCardMap.keySet();
		Iterator<Integer> playerIterator = playerSet.iterator();
		while (playerIterator.hasNext()) {
			int playerId = playerIterator.next();
			List<Card> playersCardsList = playerCardMap.get(playerId);
			System.out.println(" ---- ");
			for (Card card : playersCardsList) {
				System.out.println(playerId + " >>>>>>>> " + card.getCategory()
						+ " " + card.getValue());
			}
		}
	}

}
